/*
Define the move_right function without js interpreter. 
*/

/*========================ROBOT control blocks======================*/
Blockly.JavaScript["print_hello"] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = "print_hello();\n"; // call an API function
  // TODO: Change ORDER_NONE to the correct strength.
  return code;
};

Blockly.JavaScript["blink_led"] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = "blink_led();\n"; // call an API function
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['robot_go_forward'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("go_forward");\n';
  return code;
};

Blockly.JavaScript['robot_go_backward'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("go_backward");\n';
  return code;
};

Blockly.JavaScript['robot_turn_left'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("turn_left");\n';
  return code;
};

Blockly.JavaScript['robot_turn_right'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("turn_right");\n';
  return code;
};

Blockly.JavaScript['robot_shake_head'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("shake_head");\n';
  return code;
};

Blockly.JavaScript['robot_bounce'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("bounce");\n';
  return code;
};

Blockly.JavaScript['robot_wobble'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("wobble");\n';
  return code;
};

Blockly.JavaScript['robot_tap_feet_left'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("tap_feet_left");\n';
  return code;
};

Blockly.JavaScript['robot_tap_feet_right'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("tap_feet_right");\n';
  return code;
};

Blockly.JavaScript['robot_shake_legs'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'robotMove("shake_legs");\n';
  return code;
};
/*=======================Character action blocks===================== */

Blockly.JavaScript['move_right_in_x_second'] = function(block) {
  var number_second = block.getFieldValue('second');
  // TODO: Assemble JavaScript into code variable.
  var code = 'moveRightInXSecond('+number_second+');\n';
  return code;
};

Blockly.JavaScript["move_right"] = function(block) {
  // get the shouldJump boolean for character to jump or not
  var shouldJump = Blockly.JavaScript.valueToCode(
    block,
    "shouldJump",
    Blockly.JavaScript.ORDER_ATOMIC
  );
  // turn it to a boolean or the block doesn't work right
  x = Boolean(shouldJump);
  // call the function defined for the interpreter initInterpreterGoRight()
  var code = "goRightBlock(" + x + ");\n"; // make sure to keep the \n or it will not work correctly
  return code;
};

Blockly.JavaScript["move_left"] = function(block) {
  // get the shouldJump boolean for character to jump or not
  var shouldJump = Blockly.JavaScript.valueToCode(
    block,
    "shouldJump",
    Blockly.JavaScript.ORDER_ATOMIC
  );
  // turn it to a boolean or the block doesn't work right
  x = Boolean(shouldJump);
  // call the function defined for the interpreter initInterpreterGoRight()
  var code = "goLeftBlock(" + x + ");\n"; // make sure to keep the \n or it will not work correctly
  return code;
};

Blockly.JavaScript['jump_left'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'jumpToDirection("left");\n';
  return code;
};
Blockly.JavaScript['jump_right'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'jumpToDirection("right");\n';
  return code;
};

/* function does not do anything, but blockly complained without it. */
Blockly.JavaScript["jump"] = function(block) {
  var code = true;
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript["display_text"] = function(block) {
  //var seconds = Number(block.getFieldValue('SECONDS'));
  var string = Blockly.JavaScript.valueToCode(
    block,
    "NAME",
    Blockly.JavaScript.ORDER_ATOMIC
  );
  string = string ? string.toString() : "";
  x = String(string);
  var code = "displayText(" + x + ");\n"; // defined as sync function
  return code;
};
