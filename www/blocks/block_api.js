/*==========================ROBOT action blocks==========================================*/
// block "print_hello"
function initInterpreterPrintHello(interpreter, scope) {
  print_hello = interpreter.createAsyncFunction(callback => {
    alert('hello') // set the action for API pritn hello
  })
  interpreter.setProperty(scope, 'print_hello', print_hello) // recieve API function print_hello
}

// block "blink_led"
function initInterpreterBlinkLed(interpreter, scope) {
  blink_led = interpreter.createAsyncFunction(callback => {
    bluetoothSerial.write('<FW>')
    // alert('hello')
    setTimeout(() => {
      callback()
    }, 3000) // wait for the robot done action
  })
  interpreter.setProperty(scope, 'blink_led', blink_led) // recieve API function blink_led
}

// blocks robot_*
function initInterpreterRobotMove(interpreter, scope) {
  robotMove = interpreter.createAsyncFunction(async (action, callback) => {
    if (action == 'go_forward') {
      bluetoothSerial.write('<FW>')
    } else if (action == 'go_backward') {
      bluetoothSerial.write('<BW>')
    } else if (action == 'turn_left') {
      bluetoothSerial.write('<LT>')
    } else if (action == 'turn_right') {
      bluetoothSerial.write('<RT>')
    } else if (action == 'shake_head') {
      bluetoothSerial.write('<SX>')
    } else if (action == 'bounce') {
      bluetoothSerial.write('<BX>')
    } else if (action == 'wobble') {
      bluetoothSerial.write('<WX>')
    } else if (action == 'tap_feet_left') {
      bluetoothSerial.write('<TY>')
    } else if (action == 'tap_feet_right') {
      bluetoothSerial.write('<TZ>')
    } else if (action == 'shake_legs') {
      bluetoothSerial.write('<LX>')
    }
    await bluetoothSerial.readUntil(
      '\n',
      function(data) {
        console.log(data)
      },
      function(fail) {
        console.log(fail)
      }
    )
    setTimeout(() => {
      callback()
    }, 4000) // wait for the robot done action
  })
  interpreter.setProperty(scope, 'robotMove', robotMove)
}

/*=============================Character action blocks==========================================*/

// block 'move_right_in_x_second'
function initInterpreterMoveRightInXSecond(interpreter, scope) {
  moveRightInXSecond = interpreter.createNativeFunction(time => {
    let second = time * 1000
    moveRightX = second
  })
  interpreter.setProperty(scope, 'moveRightInXSecond', moveRightInXSecond) // recieve API function moveRightInXSecond
}

// block "move_right"
function initInterpreterGoRight(interpreter, scope) {
  // Ensure function name does not conflict with variable names.

  // uses to time outs, terminates the upward motion before termination moving right
  Blockly.JavaScript.addReservedWords('goRightBlock')
  var wrapper = interpreter.createAsyncFunction(function(x, callback) {
    goToTheRight = true
    goJump = x
    if (goJump == true) {
      setTimeout(function() {
        goJump = false // kill the upward motion first
        setTimeout(function() {
          // next stop moving to the right
          goToTheRight = false
          callback()
        }, 300)
      }, 900)
    } else {
      setTimeout(function() {
        goToTheRight = false
        callback()
      }, 300)
    }
  })
  interpreter.setProperty(scope, 'goRightBlock', wrapper)
}

// block "move_left"
function initInterpreterGoLeft(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  // uses to time outs, terminates the upward motion before termination moving right
  Blockly.JavaScript.addReservedWords('goLeftBlock')
  var wrapper = interpreter.createAsyncFunction(function(x, callback) {
    goToTheLeft = true
    goJump = x
    if (goJump == true) {
      setTimeout(function() {
        goJump = false // kill the upward motion first
        setTimeout(function() {
          // next stop moving to the left
          goToTheLeft = false
          callback()
        }, 300)
      }, 900)
    } else {
      setTimeout(function() {
        goToTheLeft = false
        callback()
      }, 300)
    }
  })
  interpreter.setProperty(scope, 'goLeftBlock', wrapper)
}

// block jump_left/jump_right
function initInterpreterJumpToDirection(interpreter, scope) {
  jumptoDirection = interpreter.createAsyncFunction((direction, callback) => {
    direction == 'left' ? goToTheLeft = true : goToTheRight = true
    goJump = true
    setTimeout(function() {
      goJump = false // kill the upward motion first
      setTimeout(function() {
        // next stop moving to the left
        direction == 'left' ? goToTheLeft = false : goToTheRight = false
        callback()
      }, 300)
    }, 900)
  })
  interpreter.setProperty(scope, 'jumpToDirection', jumptoDirection)
}

// block "display_text"
function initInterpreterDisplayText(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords('displayText')

  var wrapper = interpreter.createAsyncFunction(function(x, callback) {
    displayText.setText(x)
    setTimeout(callback, 10)
  })
  interpreter.setProperty(scope, 'displayText', wrapper)
}
