function goRight() {
  player.setVelocityX(100);
  player.anims.play("right", true);
}

function goLeft() {
  player.setVelocityX(-100);
  player.anims.play("left", true);
}

/* added this function for easy testing */
function myClick(el) {
  player.animations.add("run");
  player.animations.play("run", 15, true);
}
