// block "print_hello"
function initInterpreterPrintHello(interpreter, scope) {
  print_hello = interpreter.createNativeFunction(() => {
    alert("hello"); // set the action for API pritn hello
  });
  interpreter.setProperty(scope, "print_hello", print_hello); // recieve API function print_hello
}

// block "move_right"
function initInterpreterGoRight(interpreter, scope) {
  // Ensure function name does not conflict with variable names.

  // uses to time outs, terminates the upward motion before termination moving right
  Blockly.JavaScript.addReservedWords("goRightBlock");
  var wrapper = interpreter.createAsyncFunction(function(x, callback) {
    goToTheRight = true;
    goJump = x;
    if (goJump == true) {
      setTimeout(function() {
        goJump = false; // kill the upward motion first
        setTimeout(function() {
          // next stop moving to the right
          goToTheRight = false;
          callback();
        }, 300);
      }, 900);
    } else {
      setTimeout(function() {
        goToTheRight = false;
        callback();
      }, 300);
    }
  });
  interpreter.setProperty(scope, "goRightBlock", wrapper);
}

// block "move_left"
function initInterpreterGoLeft(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  // uses to time outs, terminates the upward motion before termination moving right
  Blockly.JavaScript.addReservedWords("goLeftBlock");
  var wrapper = interpreter.createAsyncFunction(function(x, callback) {
    goToTheLeft = true;
    goJump = x;
    if (goJump == true) {
      setTimeout(function() {
        goJump = false; // kill the upward motion first
        setTimeout(function() {
          // next stop moving to the left
          goToTheLeft = false;
          callback();
        }, 300);
      }, 900);
    } else {
      setTimeout(function() {
        goToTheLeft = false;
        callback();
      }, 300);
    }
  });
  interpreter.setProperty(scope, "goLeftBlock", wrapper);
}

// block "display_text"
function initInterpreterDisplayText(interpreter, scope) {
  // Ensure function name does not conflict with variable names.
  Blockly.JavaScript.addReservedWords("displayText");

  var wrapper = interpreter.createAsyncFunction(function(x, callback) {
    displayText.setText(x);
    setTimeout(callback, 10);
  });
  interpreter.setProperty(scope, "displayText", wrapper);
}
