/* Generate blocks with https://blockly-demo.appspot.com/static/demos/blockfactory/index.html */

/*=================================ROBOT blocks============================================*/
Blockly.Blocks["print_hello"] = {
  init: function() {
    this.appendDummyInput().appendField("print hello");
    this.setOutput(true, null);
    this.setColour(230);
    this.setNextStatement(true)
    this.setPreviousStatement(true)
  }
};

Blockly.Blocks["blink_led"] = {
  init: function() {
    this.appendDummyInput().appendField("Blink LED");
    this.setOutput(true, null);
    this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_go_forward'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Đi thẳng");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_go_backward'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Đi lùi");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_turn_left'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Quay trái");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_turn_right'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Quay phải");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_shake_head'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Lắc đầu");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_bounce'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Nhún người");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_wobble'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Lắc người");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_tap_feet_left'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Dậm chân trái");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_tap_feet_right'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Dậm chân phải");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks['robot_shake_legs'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Lắc chân");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

/*=================================Character action blocks===============================*/

Blockly.Blocks['move_right_in_x_second'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Di chuyen qua phải trong")
        .appendField(new Blockly.FieldNumber(0, 0), "second")
        .appendField("giây");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(315);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};

Blockly.Blocks["move_right"] = {
  init: function() {
    this.appendDummyInput().appendField("Di chuyển qua phải");
    // this.appendValueInput("shouldJump")
    //   .setCheck("Boolean")
    //   .setAlign(Blockly.ALIGN_RIGHT)
    //   .appendField("Có nhảy không?");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks["move_left"] = {
  init: function() {
    this.appendDummyInput().appendField("Di chuyển qua trái");
    // this.appendValueInput("shouldJump")
    //   .setCheck("Boolean")
    //   .setAlign(Blockly.ALIGN_RIGHT)
    //   .appendField("Có nhảy không?");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['jump_left'] = {
  init: function() {
    this.appendDummyInput().appendField("Nhảy qua trái");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(165);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['jump_right'] = {
  init: function() {
    this.appendDummyInput().appendField("Nhảy qua phải");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(165);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks["jump"] = {
  init: function() {
    this.appendDummyInput().appendField("Nhảy");
    this.setOutput(true, "Boolean");
    this.setColour(330);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks["display_text"] = {
  init: function() {
    this.appendDummyInput().appendField("In câu chào");
    this.appendValueInput("NAME").setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};
